# CORS proxy service

CORS proxy service help resolve CORS errors by acting as middlemen between the client's browser and the server hosting the content.

## Usage

```html
<img src="https://<host>/proxy?url=<resource URL>">
```

### Example

In this example the proxy service is hosted on Sides development cluster:

```html
<img src="https://example.com/proxy?url=https://www.mollie.com/external/icons/payment-methods/sofort.png">
```
