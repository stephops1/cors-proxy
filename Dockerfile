FROM golang:1.20 AS build-env

LABEL maintainer="steph@stephops.one"
LABEL org.label-schema.schema-version="0.1.1"
LABEL org.label-schema.name="CORS proxy service"
LABEL org.label-schema.description="This CORS proxy service help resolve CORS errors by acting as middlemen between the client's browser and the server hosting the content"
LABEL org.label-schema.vcs-url="https://gitlab.com/stephops1/cors-proxy"

WORKDIR /app
ENV CGO_ENABLED=0

COPY go.mod .
RUN go mod download

COPY . .

RUN go build -o cors-proxy .

FROM alpine:latest
WORKDIR /app
COPY --from=build-env /app/cors-proxy /cors-proxy
EXPOSE 8080

CMD ["/cors-proxy"]
