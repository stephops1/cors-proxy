package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

func health(w http.ResponseWriter, r *http.Request) {
	data := map[string]interface{}{
		"status": "ok",
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Set the Content-Type header to indicate JSON
	w.Header().Set("Content-Type", "application/json")

	// Write the JSON data to the response writer
	w.Write(jsonData)
}

func handleProxyRequest(w http.ResponseWriter, r *http.Request) {
	// Extract the URL parameter from the query string
	targetURL := r.URL.Query().Get("url")

	if targetURL == "" {
		http.Error(w, "Missing 'url' parameter", http.StatusBadRequest)
		return
	}

	// Allow only requests to www.mollie.com
	pref := "https://www.mollie.com"
	if !strings.HasPrefix(targetURL, pref) {
		http.Error(w, "Only requests to mollie.com are allowed", http.StatusBadRequest)
		return
	}

	// Create a new request to the target URL
	targetReq, err := http.NewRequest(r.Method, targetURL, r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Copy headers from the original request to the target request
	for key, values := range r.Header {
		for _, value := range values {
			targetReq.Header.Add(key, value)
		}
	}

	// Perform the request to the target server
	client := &http.Client{}
	resp, err := client.Do(targetReq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	// Read the response from the target server
	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Set response headers to allow CORS
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")

	// Write the response from the target server to the client
	w.WriteHeader(resp.StatusCode)
	w.Write(responseBody)
}

func main() {
	http.HandleFunc("/", health)
	http.HandleFunc("/health", health)
	http.HandleFunc("/proxy", handleProxyRequest)

	port := 8080
	fmt.Printf("CORS proxy server is running on :%d...\n", port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		fmt.Println(err)
	}
}
